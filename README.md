**Experis Academy, Norway**

**Authors:**

* Odd Martin Hansen
* Azadeh Karimi
* H�kon R�en

# Task 12 - Eight Queens Problem

### Task 12: Group Task - Part 1

* For groups of 2 to 3 people (not solo)
* Each person hands in on Moodle, list all team members 

 Watch this video:
* https://www.youtube.com/watch?v=jPcBU0Z2Hj8

Read about algebraic notation:
* https://en.wikipedia.org/wiki/Algebraic_notation_(chess)

### Task 12: Group Task - Part 2
* Write a program that takes in a co-ordinate on the board
* It must then tell me if it is possible to complete the 8-
queens problem with a queen at the chosen location
* If it is possible it must print a board (in console) that shows
one such possible solution


# The Algorithm

The algorithm is built around a strategy for moving left to right and placing one queen in each column. 


### The Board

The board is represented by a 2 dimensional array of integers of value 0 or 1
* 1 - Represents a Queen
* 0 - Represents an empty square


### Starting column
Starting column is at index 1 if the users Queen is placed in the first collumn.

For all other user chosen columns the start columns index is 0.


### Step by step

```
1. Place a queen in the upper left corner of the board
2. Move one step to the right
3. Start checking down that column
	3.1 If we reach the column of the user selected Queen position repeat step 2.
	3.2 Place a queen in the first avaliable square
	3.3 Repeat step 2 until we reach a column with no possible squares to place a queen
4. When we meet a column with no open square, move one column left 
	4.1 Remove the queen 
	4.2 Go to step 3 but don't start from the top, start from where the old queen used to be.
	4.3 Repeat until solution is found or we reach the bottom of the starting column.
```